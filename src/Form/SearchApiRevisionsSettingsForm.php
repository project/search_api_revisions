<?php
namespace Drupal\search_api_revisions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SearchApiRevisionsSettingsForm extends ConfigFormBase {

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['search_api_revisions.settings'];
  }

  /**
   * @inheritDoc
   */
  public function getFormId() {
    return 'search_api_revisions_settings_form';
  }

  /**
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('search_api_revisions.settings');

    $form = [
      '#type' => 'details',
      '#title' => $this->t('Search api revisions settings', [], ['context' => 'search_api_revisions']),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];

    $form['queue'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Queue settings'),
    ];

    $form['queue']['items_update_queue'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update revisions in queue', [], ['context' => 'search_api_revisions']),
      '#default_value' => $config->get('queue')['items_update_queue'] ?? FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $queue = $form_state->getValue('queue');

    $this->config('search_api_revisions.settings')
      ->set('queue', $queue)
      ->save();
    parent::submitForm($form, $form_state);
  }

}
